#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Код               = Параметры.Код;
	Наименование      = Параметры.Наименование;
	Описание          = Параметры.Описание;
	КодПроекта        = Параметры.КодПроекта;
	ДанныеАвторизации = Параметры.ДанныеАвторизации;
	Родитель          = Параметры.Родитель;
	СписокРодителей   = Параметры.СписокРодителей;
	
	Если СписокРодителей.Количество() > 0 Тогда
		
		Для Каждого СтрокаТЧ Из СписокРодителей Цикл
			Элементы.Родитель.СписокВыбора.Добавить(СтрокаТЧ.Значение,
				СтрокаТЧ.Представление, СтрокаТЧ.Пометка, СтрокаТЧ.Картинка);
		КонецЦикла;
		
	КонецЕсли;

КонецПроцедуры

#КонецОбласти


#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Сохранить(Команда)
	
	Если Не ПроверитьЗаполнение() Тогда
		Возврат;
	КонецЕсли;
	
	Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаДлительноеОжидание;
	ДлительнаяОперация = НачатьСохранениеЗоны();
	
	ПараметрыОжидания = ДлительныеОперацииКлиент.ПараметрыОжидания(ЭтотОбъект);
	ПараметрыОжидания.ВыводитьОкноОжидания = Ложь;
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПослеСохраненияЗоны", ЭтотОбъект);
	ДлительныеОперацииКлиент.ОжидатьЗавершение(ДлительнаяОперация, ОписаниеОповещения, ПараметрыОжидания);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ПослеСохраненияЗоны(Результат, ДополнительныеПараметры) Экспорт
	
	Элементы.ГруппаСтраницы.ТекущаяСтраница = Элементы.ГруппаОписание;
	
	Если Результат = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если Результат.Статус = "Выполнено" Тогда
		Ответ = ПолучитьИзВременногоХранилища(Результат.АдресРезультата);
		Если Ответ.Выполнено = Истина Тогда	
			ОповеститьОВыборе(Ответ);
		Иначе
			ОбщегоНазначенияКлиент.СообщитьПользователю(Ответ.ОписаниеОшибки);
		КонецЕсли;
	Иначе
		ТекстОшибки = СтрШаблон(НСтр("ru = 'Во время добавления зоны произошла ошибка:
                                      |%1'"), Результат.ПодробноеПредставлениеОшибки);
		ОбщегоНазначенияКлиент.СообщитьПользователю(ТекстОшибки);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция НачатьСохранениеЗоны()
	
	Инфо = Новый Структура;
	Инфо.Вставить("Код"         , Код);
	Инфо.Вставить("Описание"    , Описание);
	Инфо.Вставить("Наименование", Наименование);
	Инфо.Вставить("Родитель"    , Родитель);
	
	ПараметрыВыполнения = ДлительныеОперации.ПараметрыВыполненияФункции(УникальныйИдентификатор);
	ПараметрыВыполнения.ЗапуститьВФоне = Истина;
	
	Возврат ДлительныеОперации.ВыполнитьФункцию(ПараметрыВыполнения, "сдм_РаботаСПродюсером.ДобавитьОбъект",
		ДанныеАвторизации, КодПроекта, инфо);
	
КонецФункции

#КонецОбласти