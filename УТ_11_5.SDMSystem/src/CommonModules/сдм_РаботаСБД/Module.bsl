
#Область ПрограммныйИнтерфейс

// Запросить данные из истории устройства
// 
// Параметры:
//  ДанныеАвторизации - Структура - данные авторизации
//  КодПроекта        - Строка - код проекта в системе
//  ДатаНачала        - Дата - дата начала отбора 
//  ДатаОкончания     - Дата - дата окончания отбора
//  Периодичность     - ПеречислениеСсылка.сдм_Периодичность - периодичность данных
//  ТаблицаДанных     - ТаблицаЗначений - таблица с устройствами и codeDatabase
//  ЕдиницаИзмерения  - Структура - информация о единицах измерения
// 
// Возвращаемое значение:
//  ТаблицаЗначений - Запросить данные устройств:
// 		* codeDataBase - Строка - идентификатор параметра
// 		* registrationTime - Дата - дата события
// 		* valueMax - Число - максимальное значение
// 		* valueMin - Число - минимальное значение
// 		* Запись - Булево - реальная запись в БД
//
Функция ЗапроситьДанныеУстройств(ДанныеАвторизации, Знач КодПроекта, Знач ДатаНачала, Знач ДатаОкончания, Знач Периодичность, Знач ТаблицаДанных, Знач ЕдиницаИзмерения) Экспорт
	
	ТаблицаРезультат = Новый ТаблицаЗначений;
	ТаблицаРезультат.Колонки.Добавить("codeDataBase", Новый ОписаниеТипов("Строка",,,, Новый КвалификаторыСтроки(500)));
	ТаблицаРезультат.Колонки.Добавить("registrationTime", Новый ОписаниеТипов("Дата"));
	ТаблицаРезультат.Колонки.Добавить("valueMax", Новый ОписаниеТипов("Число"));
	ТаблицаРезультат.Колонки.Добавить("valueMin", Новый ОписаниеТипов("Число"));
	ТаблицаРезультат.Колонки.Добавить("Запись", Новый ОписаниеТипов("Булево"));
	
	СтрокаПериодичность = "second";
	Если Периодичность = Перечисления.сдм_Периодичность.Секунда Тогда
		СтрокаПериодичность = "second";
	ИначеЕсли Периодичность = Перечисления.сдм_Периодичность.Минута Тогда
		СтрокаПериодичность = "minute";
	ИначеЕсли Периодичность = Перечисления.сдм_Периодичность.Час Тогда
		СтрокаПериодичность = "hour";
	ИначеЕсли Периодичность = Перечисления.сдм_Периодичность.День Тогда
		СтрокаПериодичность = "day";
	ИначеЕсли Периодичность = Перечисления.сдм_Периодичность.Месяц Тогда
		СтрокаПериодичность = "month";
	КонецЕсли;
	
	Формула = сдм_КонвертацияЗначенийПовтИсп.ФормулаЕдиницыИзмерения(ЕдиницаИзмерения.essence, ЕдиницаИзмерения.measure).formulaOUT;
	
	Устройства = ТаблицаДанных.Скопировать();
	Устройства.Свернуть("globalId"); 
	Для Каждого КодУстройства Из Устройства Цикл
	
		СписокИдентификаторовПараметров = ТаблицаДанных.Скопировать(Новый Структура("globalId", КодУстройства.globalId)).ВыгрузитьКолонку("CodeDatabase");
		
		Данные = Новый Структура;
		Данные.Вставить("DateStart"    , ЗаписатьДатуJSON(ДатаНачала, ФорматДатыJSON.ISO, ВариантЗаписиДатыJSON.ЛокальнаяДата));
		Данные.Вставить("DateStop"     , ЗаписатьДатуJSON(ДатаОкончания, ФорматДатыJSON.ISO, ВариантЗаписиДатыJSON.ЛокальнаяДата));
		Данные.Вставить("CodeDataBases", СписокИдентификаторовПараметров);
		Данные.Вставить("formula"      , Формула);
		Данные.Вставить("timeZona"     , ЧасовойПоясСеанса());
		Данные.Вставить("interval"     , СтрокаПериодичность);
		
		Если Периодичность = Перечисления.сдм_Периодичность.Час Или Периодичность = Перечисления.сдм_Периодичность.День
			Или Периодичность = Перечисления.сдм_Периодичность.Месяц Тогда
			Запрос = сдм_РаботаСПродюсером.ЗапросMQTT("SDM.DB.Inf.Hour.Group");
		Иначе
			Запрос = сдм_РаботаСПродюсером.ЗапросMQTT("SDM.DB.Inf.Record.Group");
		КонецЕсли;
		Запрос.params = Данные;
		Запрос.Вставить("endPoint", "inf");
		Результат = сдм_РаботаСПродюсером.ЗапросКУстройству(ДанныеАвторизации, КодПроекта, КодУстройства.globalId, Запрос,, "registrationTime");
		
		Если Не Результат.Выполнено Тогда
			ВызватьИсключение
				Результат.ОписаниеОшибки;
		КонецЕсли;
		
		Если ЗначениеЗаполнено(Результат.Данные) Тогда
			Ошибка = Неопределено;
			Если Результат.Данные.Свойство("error", Ошибка) И ЗначениеЗаполнено(Ошибка) Тогда
				ВызватьИсключение Ошибка.message;
			КонецЕсли;
			
			Ответ = Неопределено;
			Если Результат.Данные.Свойство("result", Ответ) И ЗначениеЗаполнено(Ответ) Тогда
				
				Если Ответ.Свойство("records") И ЗначениеЗаполнено(Ответ.records) Тогда
					Для Каждого СтрокаТЧ Из Ответ.records Цикл
						НоваяСтрока = ТаблицаРезультат.Добавить();
						ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаТЧ);
						НоваяСтрока.Запись = Истина;
					КонецЦикла;
					
				КонецЕсли;
				
			КонецЕсли
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат ТаблицаРезультат;
	
КонецФункции

// Запросить полные данные с устройства.
// 
// Параметры:
//  ДанныеАвторизации - Структура - данные авторизации
//  КодПроекта        - Строка - код проекта в системе
//  ДатаНачала        - Дата - дата начала отбора 
//  ДатаОкончания     - Дата - дата окончания отбора
//  ТаблицаДанных     - ТаблицаЗначений - таблица с устройствами и codeDatabase
//  ЕдиницаИзмерения  - Структура - информация о единицах измерения
//  Фильтр            - Структура - фильтр для отбора записей
// 
// Возвращаемое значение:
//  ТаблицаЗначений - Запросить полные данные устройств:
// 		* codeDataBase - Строка - идентификатор параметра
// 		* registrationTime - Дата - дата события
// 		* value - Число - значение параметра
// 		* levelAlarm - Число - уровень тревоги
// 		* Запись - Булево - реальная запись в БД
//
Функция ЗапроситьПолныеДанныеУстройств(ДанныеАвторизации, Знач КодПроекта, Знач ДатаНачала, Знач ДатаОкончания, Знач ТаблицаДанных, Знач ЕдиницаИзмерения = Неопределено, Знач Фильтр = "") Экспорт
	
	ТаблицаРезультат = Новый ТаблицаЗначений;
	ТаблицаРезультат.Колонки.Добавить("codeDataBase", Новый ОписаниеТипов("Строка",,,, Новый КвалификаторыСтроки(500)));
	ТаблицаРезультат.Колонки.Добавить("registrationTime", Новый ОписаниеТипов("Дата"));
	ТаблицаРезультат.Колонки.Добавить("value", Новый ОписаниеТипов("Число"));
	ТаблицаРезультат.Колонки.Добавить("levelAlarm", Новый ОписаниеТипов("Число"));
	ТаблицаРезультат.Колонки.Добавить("isAlarm", Новый ОписаниеТипов("Число"));
	ТаблицаРезультат.Колонки.Добавить("timeDeposit", Новый ОписаниеТипов("Число"));
	ТаблицаРезультат.Колонки.Добавить("Запись", Новый ОписаниеТипов("Булево"));
	
	Если ЕдиницаИзмерения = Неопределено Тогда
		Формула = "z";
	Иначе
		Формула = сдм_КонвертацияЗначенийПовтИсп.ФормулаЕдиницыИзмерения(ЕдиницаИзмерения.essence, ЕдиницаИзмерения.measure).formulaOUT;
	КонецЕсли;
	
	Устройства = ТаблицаДанных.Скопировать();
	Устройства.Свернуть("globalId"); 
	Для Каждого КодУстройства Из Устройства Цикл
		
		Если Не ЗначениеЗаполнено(КодУстройства.globalId) Тогда
			Продолжить;
		КонецЕсли;
		
		СписокИдентификаторовПараметров = ТаблицаДанных.Скопировать(Новый Структура("globalId", КодУстройства.globalId)).ВыгрузитьКолонку("CodeDatabase");
		
		Данные = Новый Структура;
		Данные.Вставить("DateStart"    , ЗаписатьДатуJSON(ДатаНачала, ФорматДатыJSON.ISO, ВариантЗаписиДатыJSON.ЛокальнаяДата));
		Данные.Вставить("DateStop"     , ЗаписатьДатуJSON(ДатаОкончания, ФорматДатыJSON.ISO, ВариантЗаписиДатыJSON.ЛокальнаяДата));
		Данные.Вставить("CodeDataBases", СписокИдентификаторовПараметров);
		Данные.Вставить("formula"      , Формула);
		Данные.Вставить("timeZona"     , ЧасовойПоясСеанса());
		Данные.Вставить("filter"       , Фильтр);
		
		Запрос = сдм_РаботаСПродюсером.ЗапросMQTT("SDM.DB.Inf.Record");
		Запрос.params = Данные;
		Запрос.Вставить("endPoint", "inf");
		Результат = сдм_РаботаСПродюсером.ЗапросКУстройству(ДанныеАвторизации, КодПроекта, КодУстройства.globalId, Запрос,, "registrationTime");
		
		Если Не Результат.Выполнено Тогда
			ВызватьИсключение
				Результат.ОписаниеОшибки;
		КонецЕсли;
		
		Если ЗначениеЗаполнено(Результат.Данные) Тогда
			Ошибка = Неопределено;
			Если Результат.Данные.Свойство("error", Ошибка) И ЗначениеЗаполнено(Ошибка) Тогда
				ВызватьИсключение Ошибка.message;
			КонецЕсли;
			
			Ответ = Неопределено;
			Если Результат.Данные.Свойство("result", Ответ) И ЗначениеЗаполнено(Ответ) Тогда
				
				Если Ответ.Свойство("records") И ЗначениеЗаполнено(Ответ.records) Тогда
					Для Каждого СтрокаТЧ Из Ответ.records Цикл
						НоваяСтрока = ТаблицаРезультат.Добавить();
						ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаТЧ);
						НоваяСтрока.Запись = Истина;
					КонецЦикла;
					
				КонецЕсли;
				
			КонецЕсли
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат ТаблицаРезультат;
КонецФункции

Функция ВосстановитьДатыВСобытиях(ДанныеАвторизации, Знач КодПроекта, Знач ДатаНачала, Знач ДатаОкончания, Знач Storage, Знач CodeDatabase, Знач Отклонение) Экспорт
	
	Данные = Новый Структура;
	Данные.Вставить("DateStart"    , ЗаписатьДатуJSON(ДатаНачала, ФорматДатыJSON.ISO, ВариантЗаписиДатыJSON.ЛокальнаяДата));
	Данные.Вставить("DateStop"     , ЗаписатьДатуJSON(ДатаОкончания, ФорматДатыJSON.ISO, ВариантЗаписиДатыJSON.ЛокальнаяДата));
	Данные.Вставить("CodeDataBases", CodeDatabase);
	Данные.Вставить("timeZona"     , ЧасовойПоясСеанса());
	Данные.Вставить("offset"       , Отклонение);
	
	Запрос = сдм_РаботаСПродюсером.ЗапросMQTT("SDM.DB.Inf.RecoveryRecord");
	Запрос.params = Данные;
	Запрос.Вставить("endPoint", "inf");
	Запрос.Вставить("WaitTime", 300);
	Результат = сдм_РаботаСПродюсером.ЗапросКУстройству(ДанныеАвторизации, КодПроекта, Storage, Запрос);
	
	Если Не Результат.Выполнено Тогда
		ВызватьИсключение
			Результат.ОписаниеОшибки;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Результат.Данные) Тогда
		Ошибка = Неопределено;
		Если Результат.Данные.Свойство("error", Ошибка) И ЗначениеЗаполнено(Ошибка) Тогда
			ВызватьИсключение Ошибка.message;
		КонецЕсли;
		
		Ответ = Неопределено;
		Если Результат.Данные.Свойство("result", Ответ) И ЗначениеЗаполнено(Ответ) Тогда
			Возврат Истина;
		КонецЕсли
	КонецЕсли;
	
	Возврат Ложь;
	
КонецФункции

#КонецОбласти
